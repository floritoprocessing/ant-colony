class Obstacle {
  
  Vec center;
  Vec[] corner;
  float rotation;
  
  Obstacle(float x, float y) {
    corner = new Vec[Preset.OBSTACLE_CORNER_POINTS];
    for (int i=0;i<corner.length;i++) {
      center = new Vec(x,y,0);
      Vec va = new Vec(random(Preset.OBSTACLE_MIN_RADIUS,Preset.OBSTACLE_MAX_RADIUS),0,0);
      va.rotZ(2.0*PI*(i/(float)Preset.OBSTACLE_CORNER_POINTS));
      corner[i] = vecAdd(center,va);
    }
    rotation = random(1.0)<0.5?-random(Preset.OBSTACLE_ROT_SPEED_MIN,Preset.OBSTACLE_ROT_SPEED_MAX):random(Preset.OBSTACLE_ROT_SPEED_MIN,Preset.OBSTACLE_ROT_SPEED_MAX);
  }
  
  void rotateSlowly() {
    for (int i=0;i<corner.length;i++) {
      corner[i].sub(center);
      corner[i].rotZ(rotation);
      corner[i].add(center);
    }
  }
  
  void toScreen() {
    fill(Preset.OBSTACLE_COLOR);
    noStroke();
    beginShape(POLYGON);
    for (int i=0;i<corner.length;i++) vecVertex(corner[i]);
    endShape();
  }
  
}
