/*****************************************************/
/*                    AntColonyV5                    */
/*****************************************************/

Ant[] ant;
Obstacle[] obstacle;
Base home;
Base food;
Ground ground;


void setup() {
  size(500,500,P3D);
  background(Preset.COLOR_BACKGROUND);

  ground = new Ground(width,height);

  /*****************************************************
  /* SETUP OBSTACLES: */

  obstacle = new Obstacle[Preset.OBSTACLE_AMOUNT];
  for (int i=0;i<obstacle.length;i++) obstacle[i] = new Obstacle(random(0.1*width,0.9*width),random(0.1*height,0.9*height));
  for (int i=0;i<obstacle.length;i++) obstacle[i].toScreen();

  /*****************************************************/





  /*****************************************************
  /* SETUP HOME and FOOD: */

  home = new Base(makePositionOutsideObstacle(),Preset.BASE_MODE_HOME);
  food = new Base(makePositionOutsideObstacle(),Preset.BASE_MODE_FOOD);

  /*****************************************************/





  /*****************************************************/
  /* SETUP ANTS: */

  ant = new Ant[Preset.ANT_AMOUNT];
  for (int i=0;i<ant.length;i++) ant[i] = new Ant(makePositionOutsideObstacle());

  /*****************************************************/


}

void mousePressed() {
  food.setPosition(mouseX,mouseY);
}

void draw() {
  ground.toScreen();

  for (int i=0;i<obstacle.length;i++) obstacle[i].rotateSlowly();
  for (int i=0;i<obstacle.length;i++) obstacle[i].toScreen();

  home.toScreen();
  food.toScreen();

  for (int i=0;i<ant.length;i++) ant[i].move(home);
  for (int i=0;i<ant.length;i++) ant[i].toScreen();
  for (int i=0;i<ant.length;i++) ant[i].leaveTrail(ground);
}


Vec makePositionOutsideObstacle() {
  float x = random(width);
  float y = random(height);
  // position them outside the obstacles:
  while (get((int)x,(int)y)==Preset.OBSTACLE_COLOR) {
    x = random(width);
    y = random(height);
  }
  return new Vec(x,y);
}





/*
  println("public static final double SIN6P = "+Math.sin(PI/30.0)+";");//0.5000000126183913;
 println("public static final double SIN6M = "+Math.sin(-PI/30.0)+";");
 println("public static final double COS6P = "+Math.cos(PI/30.0)+";");
 println("public static final double COS6M = "+Math.cos(-PI/30.0)+";");
 */
