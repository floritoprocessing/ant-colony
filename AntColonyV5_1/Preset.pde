class Preset {
  
  
    
  /*****************************************************
  / ANT PRESETS: */
  
  // amount:
  public static final int ANT_AMOUNT = 500;
  
  // behaviour
  public static final float ANT_SPEED = 3.0;
  public static final float ANT_SPEED_MIN_FOR_HARD_TURN = 0.7;
  public static final float ANT_LOOK_AHEAD_FACTOR = 6.0;
  public static final float ANT_SLOW_DOWN_FAC = 0.8;
  public static final float ANT_HOME_URGE = 0.2;      // 0..1
  public static final int ANT_MODE_GOING_HOME = 0;
  public static final int ANT_MODE_FINDING_FOOD = 1;
  
  // display
  public static final boolean ANT_SHOW_LOOK_AHEAD = false;
  public static final int ANT_COLOR = 0xFF8C6640;
  public static final int ANT_COLOR_HARD_TURN = 0xFFFFAA80;
  public static final float ANT_DRAW_SIZE = 3.0;
  
  /*****************************************************/
  
  
  
  
  
  /*****************************************************
  / BASE PRESETS: */
  
  //mode:
  public static final int BASE_MODE_HOME = 0;
  public static final int BASE_MODE_FOOD = 1;
  
  // display:
  public static final int BASE_HOME_COLOR = 0xFF000000;
  public static final float BASE_HOME_RADIUS = 12;
  
  public static final int BASE_FOOD_COLOR = 0xFF00FF00;
  public static final float BASE_FOOD_RADIUS = 12;
  
  /*****************************************************/
  
  
  
  
  
  /*****************************************************
  / OBSTACLE PRESETS: */
  
  //form:
  public static final int OBSTACLE_AMOUNT = 40;
  public static final int OBSTACLE_CORNER_POINTS = 9;
  public static final float OBSTACLE_MIN_RADIUS = 10;
  public static final float OBSTACLE_MAX_RADIUS = 30;
  public static final int OBSTACLE_COLOR = 0xFF612E03;
  
  
  public static final float OBSTACLE_ROT_SPEED_MIN = 0.000;
  public static final float OBSTACLE_ROT_SPEED_MAX = 0.000;
  
  /*****************************************************/
  
  
  
  
  
  public static final int COLOR_BACKGROUND = 0xFFFFFFFF;
  public static final int COLOR_TRAIL = 0xFF0000;
  public static final float COLOR_TRAIL_STRENGTH = 0.1;
  
  
  Preset() {
  }
  
}
