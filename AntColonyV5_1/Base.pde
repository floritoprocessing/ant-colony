class Base {
  Vec pos = new Vec();
  int mode;
  
  Base(Vec _pos, int _mode) {
    pos.setVec(_pos);
    mode=_mode;
  }
  
  void setPosition(double x, double y) {
    pos.setVec(x,y);
  }
  
  void toScreen() {
    noStroke();
    if (mode==Preset.BASE_MODE_FOOD) {
      fill(Preset.BASE_FOOD_COLOR);
      ellipse((float)pos.x,(float)pos.y,Preset.BASE_FOOD_RADIUS,Preset.BASE_FOOD_RADIUS);
    } else {
      fill(Preset.BASE_HOME_COLOR);
      ellipse((float)pos.x,(float)pos.y,Preset.BASE_HOME_RADIUS,Preset.BASE_HOME_RADIUS);
    }
  }
  
}
