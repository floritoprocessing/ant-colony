class Ground {
  int w,h;
  PImage img;
  
  //RGB
  Ground(int _w, int _h) {
    w=_w;
    h=_h;
    img = new PImage(w,h);
    for (int i=0;i<img.pixels.length;i++) {
      img.pixels[i] = Preset.COLOR_BACKGROUND;
    }
  }
  
  void leaveTrail(int _x, int _y, float stren) {
    for (int ox=-1;ox<=1;ox++) {
      for (int oy=-1;oy<=1;oy++) {
        float dcStren=0;
        if (ox==0&&oy==0) {
          // center point
          dcStren = stren*Preset.COLOR_TRAIL_STRENGTH;
        } else {
          if (ox==0||oy==0) {
            dcStren = 0.5*stren*Preset.COLOR_TRAIL_STRENGTH;
          } else {
            dcStren = 0.35*stren*Preset.COLOR_TRAIL_STRENGTH;
          }
        }
        float bgStren = 1.0-dcStren;
        int bgCol = img.get(_x+ox,_y+oy);   
        int outCol = mixColor(Preset.COLOR_TRAIL,dcStren,bgCol,bgStren);
        img.set(_x+ox,_y+oy,outCol);
      }
    }
    
    
    //float dcStren = stren*Preset.COLOR_TRAIL_STRENGTH;
//    float bgStren = 1.0-dcStren;
//    int bgCol = img.get(_x,_y);   
//    int outCol = mixColor(Preset.COLOR_TRAIL,dcStren,bgCol,bgStren);
//    img.set(_x,_y,outCol);
  }
  
  void toScreen() {
    image(img,0,0);
  }
  
  int mixColor(int c1, float p1, int c2, float p2) {
    int c1R = c1>>16&0xFF;
    int c1G = c1>>8&0xFF;
    int c1B = c1&0xFF;
    int c2R = c2>>16&0xFF;
    int c2G = c2>>8&0xFF;
    int c2B = c2&0xFF;
    int outR = (int)(c1R*p1 + c2R*p2);
    int outG = (int)(c1G*p1 + c2G*p2);
    int outB = (int)(c1B*p1 + c2B*p2);
    int outCol = outR<<16|outG<<8|outB;
    return outCol;
  }
  
}
