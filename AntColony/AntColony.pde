

Base nest;
Base food;
Ant[] ant;
Environment environment;

boolean showEnvironment=false;
boolean showAnts=true;
boolean showBases=true;

void setup() {
  size(800,600);
 
  nest=new Base(0,width/2.0,0,height/2.0,20,color(0,0,255),true);
  food=new Base(width/2.0,width,height/2.0,height,20,color(0,255,0),false);
  environment=new Environment(width,height);
  
  initAnts(400,width,height);
  println("Press [1] and/or [2]");
}

void draw() {
  for (int i=0;i<10;i++) {
    antsUpdate(environment);
  }
  nest.listenToMouse();
  food.listenToMouse();
  environment.dissolveFerhormones(10);
  
  
  showEnvironment=false;
  showAnts=true;
  showBases=true;
  if (keyPressed) {
    if (key=='1') {showEnvironment=true; showAnts=true;}
    if (key=='2') {showEnvironment=true; showAnts=false; showBases=false;}
  }
  
  // drawing:
  background(255,255,255);
  if (showEnvironment) {environment.toScreen();}
  if (showBases) {
    nest.toScreen();
    food.toScreen();
  }
  if (showAnts) { antsToScreen(); }
}

/*
  INIT FUNCTIONS
*/

void initAnts(int n, int w, int h) {
  ant=new Ant[n];
  for (int i=0;i<n;i++) {
    if (random(1)<1.0) {
      ant[i]=new Ant(w,h,nest,food,false);
    } else {
      ant[i]=new Ant(w,h,food,nest,true);
    }
    
  }
}

/*
  UPDATE FUNCTIONS
*/

void antsUpdate(Environment env) {
  for (int i=0;i<ant.length;i++) {
    ant[i].update(env);
  }
}

void antsToScreen() {
  for (int i=0;i<ant.length;i++) {
    ant[i].toScreen();
  }
}
