class Base {
  Vec pos;
  float radius;
  color col;
  boolean dragging=false;
  Vec mouseDownPos=new Vec();
  boolean isHome=false;
  
  Base(float wMin, float wMax, float hMin, float hMax, float r, color c, boolean _isHome) {
    pos=new Vec(random(wMin+r,wMax-r),random(hMin+r,hMax-r),0);
    radius=r;
    col=c;
    isHome=_isHome;
  }
  
  Vec randomPosOnBorder() {
    float rd=random(2*PI);
    Vec out=new Vec(pos.x+radius*cos(rd),pos.y+radius*sin(rd),0);
    return out;
  }
  
  Vec randomPosOnCircle() {
    float rd=random(2*PI);
    float r=random(radius);
    Vec out=new Vec(pos.x+r*cos(rd),pos.y+r*sin(rd),0);
    return out;
  }
  
  void listenToMouse() {
    Vec mousePos=new Vec(mouseX,mouseY,0);
    if (!dragging&&mousePressed&&vecDistance(mousePos,pos)<radius) {
      dragging=true;
      // one-time mousePress event:
//      mouseDownPos.setVec(mousePos);
//      println("MOUSE DOWN: "+mouseDownPos.x+" , "+mouseDownPos.y);
    }
    
    if (dragging) {
      Vec movement=new Vec(pmouseX-mouseX,pmouseY-mouseY,0);
      pos.sub(movement);
    }
    
    if (!mousePressed) {
      dragging=false;
    }
    
  }
  
  void toScreen() {
    noStroke();
    fill(col);
    ellipseMode(CENTER);
    ellipse((float)pos.x,(float)pos.y,radius,radius);
  }
}
