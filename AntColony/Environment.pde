class Environment {
  int w;
  int h;
  double[] grid;
  
  Environment(int _w, int _h) {
    w=_w;
    h=_h;
    grid=new double[w*h];
    for (int i=0;i<w*h;i++) {
      grid[i]=0;
    }
  }
  
  void addFerhormonesOnPos(Ant a) {
    int x=(int)constrain((float)a.pos.x,0,w-1);
    int y=(int)constrain((float)a.pos.y,0,h-1);
    int i=y*w+x;
    grid[i]+=0.05;
    if (grid[i]>1) {grid[i]=1;}
  }
  
  void dissolveFerhormones(double fac) {
    for (int i=0;i<w*h;i++) {
      grid[i]-=0.0001*fac;
      if (grid[i]<0) {grid[i]=0;}
    }
  }
  
  Vec getFerhormoneDirection(Ant a,int range) {
    Vec outVec=new Vec(0,0,0);
    int bx=(int)(a.pos.x);
    int by=(int)(a.pos.y);
    for (int x=-range;x<=range;x++) {
      for (int y=-range;y<=range;y++) {
        int px=bx+x;
        int py=by+y;
        if (px>=0&&px<w&&py>=0&&py<h&&(!(x==0&&y==0))) {
          Vec dVec=new Vec(x,y,0);
          dVec.setLen(grid[py*w+px]);
          outVec.add(dVec);
        }
      }
    }
    return outVec;
  }
  
  void toScreen() {
//    double gMax=0.000001;
//    for (int i=0;i<w*h;i++) {
//      if (grid[i]>gMax) {gMax=grid[i];}
//    }
    
    for (int x=0;x<w;x++) {
      for (int y=0;y<h;y++) {
        int i=y*w+x;
//        float f=(float)(255-255*grid[i]/gMax);
        float f=(float)(255-255*grid[i]);
        set(x,y,color(f,f,f));
      }
    }
  }
}
