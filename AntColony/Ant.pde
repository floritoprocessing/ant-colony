class Ant {
  Vec pos;
  Vec mov;
  float xMax, yMax;
  Base source, target;
  color col;
  boolean leaveTrace=false;
  
  Ant(float w,float h,Base _source, Base _target, boolean _leaveTrace) {
    xMax=w-1;
    yMax=h-1;
    pos=_source.randomPosOnCircle();
    mov=rndDirVecXY(1);  // create random dir, length: 1
    source=_source;
    target=_target;
    col=target.col;
    leaveTrace=_leaveTrace;
  }
  
  void update(Environment env) {
    if (reached(target)) {
      if (!target.isHome) {leaveTrace=true;}
      //mov.rotZ(PI);
      Base temp=target;
      target=source;
      source=temp;
      col=target.col;
    }
    
    
//    /*
    // random mov:
    Vec rndVec=rndDirVecXY(0.01);
    mov.add(rndVec); 
//    */
    
    
    Vec toTarget=new Vec(target.pos.x-pos.x,target.pos.y-pos.y,0);
    if (target.isHome) {
      // mov to target:  
      toTarget.setLen(0.03); //0.05
    } else {
      toTarget.setLen(0.005);
    }
    mov.add(toTarget); 
    
    // mov to ferhormones:
    Vec toFerhormones=new Vec();
    toFerhormones=env.getFerhormoneDirection(this,3);
    toFerhormones.setLen(0.15); //0.1
    mov.add(toFerhormones);
    
    
    mov.normalize();
    pos.add(mov);
    if (pos.x<0) {pos.x=0; mov.x*=-1;}
    if (pos.x>xMax) {pos.x=xMax; mov.x*=-1;}
    if (pos.y<0) {pos.y=0; mov.y*=-1;}
    if (pos.y>yMax) {pos.y=yMax; mov.y*=-1;}
    
    if (leaveTrace) {env.addFerhormonesOnPos(this);}
  }
  
  boolean reached(Base base) {
    return (base.radius>dist((float)base.pos.x,(float)base.pos.y,(float)pos.x,(float)pos.y));
  }
  
  void toScreen() {
    noStroke();
    fill(col);
    ellipseMode(CENTER);
    ellipse((float)pos.x,(float)pos.y,2,2);
  }
}
