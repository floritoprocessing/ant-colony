class Ant {
  
  float direction=0;
  float speed=0.8;
  
  float co=0;
  float si=0;
  
  float x=0;
  float y=0;
  
  int xmax=640;
  int ymax=480;
  
  public static final int MODE_SEARCHING=0;
  public static final int MODE_GOING_HOME=1;
  int mode=0;
  
  public static final int COLOR_SEARCHING=0x606060;
  public static final int COLOR_GOING_HOME=0x40ff40;
  int col=COLOR_SEARCHING;
  
  Ant(int _xm, int _ym, Home h) {
    xmax=_xm;
    ymax=_ym;
    float[] pos=h.randomInsideCirclePosition();
    x=pos[0];
    y=pos[1];
    randomDirectionAndSpeed();
    createCoSiFromDirectionAndSpeed();
  }
  
  Ant(int _xm, int _ym) {
    xmax=_xm;
    ymax=_ym;
    x=random(xmax);
    y=random(ymax);
    randomDirectionAndSpeed();
  }
  
  void randomDirectionAndSpeed() {
    direction=random(2*PI);
    speed=random(1.0,2.0);
  }
  
  void createCoSiFromDirectionAndSpeed() {
    co=speed*cos(direction);
    si=speed*sin(direction);
  }
  
  void move(Home h, Vector fss, Ferhormones fh) {
    
    // if foodsource, eat food and change mode to going home
    for (int i=0;i<fss.size();i++) {
      Foodsource f=(Foodsource)(fss.elementAt(i));
      if (f.inRangeOf(x,y)) {
        f.reduceAmount();
        mode=MODE_GOING_HOME;
        //col=COLOR_GOING_HOME;
        col=f.getColor();
      }
    }
    
    // if home, change mode to searching
    if (h.inRangeOf(x,y)) {
      mode=MODE_SEARCHING;
      col=COLOR_SEARCHING;
    }
    
    // random movement:
    direction+=random(-0.3,0.3);
    while (direction<0) direction+=2*PI;
    while (direction>=2*PI) direction-=2*PI;
    
    // create vectors:
    co=speed*cos(direction);
    si=speed*sin(direction);
    float lx=x, ly=y;
    x+=co;
    y+=si;
    
    if (mode==MODE_GOING_HOME) {
      float dx=h.x-x;
      float dy=h.y-y;
      float r=sqrt(dx*dx+dy*dy)/speed;
      dx/=r;
      dy/=r;
      x+=dx;
      y+=dy;
      direction=atan2(y-ly,x-lx);
    }
    
    while (x<0) x+=xmax;
    while (x>=xmax) x-=xmax;
    while (y<0) y+=ymax;
    while (y>=ymax) y-=ymax;
    
    if (mode==MODE_GOING_HOME) {
      fh.addTrace((int)x,(int)y);
    }
  }
  
  void toScreen() {
    stroke(col>>16&0xff,col>>8&0xff,col&0xff);
    point(x,y);
  }
  
}
