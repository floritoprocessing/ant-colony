class Ferhormones {
  
  int xdim, ydim;
  Vector activeHormones=new Vector();
  float[][] screenArray;
  
  Ferhormones(int _xdim, int _ydim) {
    xdim=_xdim;
    ydim=_ydim;
    screenArray=new float[xdim][ydim];
    for (int x=0;x<xdim;x++) {
      for (int y=0;y<ydim;y++) {
        screenArray[x][y]=0;
      }
    }
  }
  
  void addTrace(int x, int y) {
    int[] pos=new int[2];
    pos[0]=x;
    pos[1]=y;
    activeHormones.remove(pos);
    activeHormones.add(pos);
    screenArray[x][y]+=5;
  }
  
  void toScreen() {
    for (int i=0;i<activeHormones.size();i++) {
      int[] pos=(int[])(activeHormones.elementAt(i));
      set(pos[0],pos[1],color(screenArray[pos[0]][pos[1]]));
    }
    println(activeHormones.size());
  }
}
