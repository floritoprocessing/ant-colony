class Foodsource {
  
  float amountOfFood=500;
  float x=0;
  float y=0;
  float radius, rad2;
  
  color col;
  
  Foodsource(float _x, float _y, float _amount, color _col) {
    x=_x;
    y=_y;
    amountOfFood=_amount;
    col=_col;
    updateAmountOfFood();
  }
  
  void updateAmountOfFood() {
    radius=sqrt(amountOfFood/PI);
    rad2=radius*radius;
  }
  
  boolean inRangeOf(float ax, float ay) {
    boolean out=false;
    float dx=ax-x;
    if (abs(dx)<radius) {
      float dy=ay-y;
      if (abs(dy)<radius) {
        if (rad2>(dx*dx+dy*dy)) {
          out=true;
        }
      }
    }
    return out;
  }
  
  void reduceAmount() {
    if (amountOfFood>0) amountOfFood--;
    updateAmountOfFood();
  }
  
  boolean isDead() {
    if (amountOfFood<1) {
      return true;
    } else {
      return false;
    }
  }
  
  void toScreen() {
    noStroke();
    fill(col);
    ellipseMode(CENTER);
    ellipse(x,y,radius,radius);
  }
  
  color getColor() {
    return col;
  }
  
  
}
