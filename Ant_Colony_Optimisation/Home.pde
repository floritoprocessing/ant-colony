class Home {
  float x=0;
  float y=0;
  float radius=10, rad2=100;
  color col=color(64,64,64);
  
  Home(float _x, float _y, float _r) {
    x=_x;
    y=_y;
    radius=_r;
    rad2=radius*radius;
  }
  
  float[] randomInsideCirclePosition() {
    float[] pos=new float[2];
    float rd=random(2*PI);
    float r=random(radius);
    pos[0]=x+r*cos(rd);
    pos[1]=y+r*sin(rd);
    return pos;
  }
  
  boolean inRangeOf(float ax, float ay) {
    boolean out=false;
    float dx=ax-x;
    if (abs(dx)<radius) {
      float dy=ay-y;
      if (abs(dy)<radius) {
        if (rad2>(dx*dx+dy*dy)) {
          out=true;
        }
      }
    }
    return out;
  }
  
  void toScreen() {
    noStroke();
    fill(col);
    ellipseMode(CENTER);
    ellipse(x,y,radius,radius);
  }
  
  color getColor() {
    return col;
  }
}
