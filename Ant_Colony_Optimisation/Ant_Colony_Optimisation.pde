import java.util.Vector;

float ANT_HOME_RADIUS=25;

int FOODSOURCES_AMOUNT=10;
int FOODSOURCE_MIN_SIZE=50;
int FOODSOURCE_MAX_SIZE=250;

int ANT_AMOUNT=3000;

Home home;
Ant[] ant;
Vector foodsources=new Vector();
Ferhormones ferhormones;

void setup() {
  size(800,600,P3D);
  
  // Init ferhormones:
  ferhormones=new Ferhormones(width,height);
  
  // Make Homebase for ants:
  home=new Home((0.2+random(0.6))*width,(0.2+random(0.6))*height,ANT_HOME_RADIUS);
  
  // Make Foodsources:
  for (int i=0;i<FOODSOURCES_AMOUNT;i++) {
    Foodsource f=new Foodsource((0.1+random(0.8))*width,(0.1+random(0.8))*height,random(FOODSOURCE_MIN_SIZE,FOODSOURCE_MAX_SIZE),color(128+random(128),128+random(128),128+random(128)));
    foodsources.add(f);
  }
  
  // Make Ants:
  ant=new Ant[ANT_AMOUNT];
  for (int i=0;i<ANT_AMOUNT;i++) ant[i]=new Ant(width,height);
}

void draw() {
  background(0,0,0);
  
  // draw ferhormones;
  ferhormones.toScreen();
  
  // draw homebases:
  home.toScreen();
  
  // draw foodsource:
  for (int i=0;i<foodsources.size();i++) { 
    Foodsource f=(Foodsource)(foodsources.elementAt(i));
    if (f.isDead()) foodsources.remove(f);
    f.toScreen();
   }
  
  // move and draw ants:
  for (int i=0;i<ANT_AMOUNT;i++) {
    ant[i].move(home,foodsources,ferhormones);
    ant[i].toScreen();
  }
}
