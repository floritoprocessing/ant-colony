class Ant {
  
  float speed = Preset.ANT_SPEED;
  
  Vec pos = new Vec();
  Vec mov = new Vec();
  Vec lookRight = new Vec();
  Vec lookLeft = new Vec();
  Vec aheadVec = new Vec();
  Vec futPos = new Vec();
  
  
  Ant(float x, float y) {
    pos.setVec(x,y,0);
    mov.setVec(speed,0,0);
    mov.rotZ(random(0,2*PI));
  }
  
  
  void move() {  
    if (speed<Preset.ANT_SPEED_MIN_FOR_HARD_TURN) mov.rotZ(PI);
  
    // RANDOM MOVEMENT:
    float d = random(1.0);
    if (d<0.33333) {
      mov.rotZ_6P();
    } else if (d<0.66666) {
      mov.rotZ_6M();
    }
    
    aheadVec = vecMul(mov,Preset.ANT_LOOK_AHEAD_FACTOR);
    futPos = vecAdd(pos,aheadVec);
    
    
    // CHECK FOR BOUNDARIES:
    if (futPos.x<0&&mov.x<0&&mov.y<0||futPos.x>width&&mov.x>0&&mov.y>0) { mov.rotZ_15P(); speed*=Preset.ANT_SLOW_DOWN_FAC; }
    if (futPos.y<0&&mov.y<0&&mov.x>0||futPos.y>height&&mov.y>0&&mov.x<0) { mov.rotZ_15P(); speed*=Preset.ANT_SLOW_DOWN_FAC; }
    if (futPos.x<0&&mov.x<0&&mov.y>0||futPos.x>width&&mov.x>0&&mov.y<0) { mov.rotZ_15M(); speed*=Preset.ANT_SLOW_DOWN_FAC; }
    if (futPos.y<0&&mov.y<0&&mov.x<0||futPos.y>height&&mov.y>0&&mov.x>0) { mov.rotZ_15M(); speed*=Preset.ANT_SLOW_DOWN_FAC; }
   
   
    /*
    // CHECK FOR OTHER ANTS:
    for (float dst=0;dst<Preset.ANT_LOOK_AHEAD_FACTOR;dst++) {
      Vec futPos2 = vecAdd(pos,vecMul(new Vec(mov),dst));
      if (get((int)futPos2.x,(int)futPos2.y)==Preset.ANT_COLOR) speed*=Preset.ANT_SLOW_DOWN_FAC;
    }
    */
    
    
    // CHECK FOR OBSTACLES:
    if (get((int)futPos.x,(int)futPos.y)==Preset.OBSTACLE_COLOR) {
      if (get((int)pos.x,(int)pos.y)!=Preset.OBSTACLE_COLOR) {
        
        // make vectors 15 degrees left and right of move direction
        lookRight.setVec(mov);
        lookRight.rotZ_15P();
        lookLeft.setVec(mov);
        lookLeft.rotZ_15M();
        
        float dstFac=0.0;
        boolean foundObstacle=false;
        int foundDir = 0;
        
        // by "drawing" two lines from position towards lookLeft and lookRight
        // the line that crosses the obstacle first defines the avoiding direction to take
        while (!foundObstacle&&dstFac<Preset.ANT_LOOK_AHEAD_FACTOR) {
          if (get((int)(pos.x+dstFac*lookRight.x),(int)(pos.y+dstFac*lookRight.y))==Preset.OBSTACLE_COLOR) foundDir=1;
          if (get((int)(pos.x+dstFac*lookLeft.x),(int)(pos.y+dstFac*lookLeft.y))==Preset.OBSTACLE_COLOR) foundDir=-1;
          dstFac++;
          if (foundDir!=0) { foundObstacle=true; speed*=Preset.ANT_SLOW_DOWN_FAC; }
        }
        
        if (foundDir==1) mov.rotZ_30M();
        if (foundDir==-1) mov.rotZ_30P();
        
      }
    }
    
    
   
     
    // MOVE:
    mov.setLen(speed);
    pos.add(mov);
    
    // if really hasn't managed to avoid the boundaries, jump them!
    if (pos.x<0) pos.x+=width;
    if (pos.x>=width) pos.x-=width;
    if (pos.y<0) pos.y+=height;
    if (pos.y>=height) pos.y-=height;
    
    speed = (15*speed + Preset.ANT_SPEED)/16.0;
  }
  
  
  
  
  
  void toScreen() {
    noStroke();
    if (speed<Preset.ANT_SPEED_MIN_FOR_HARD_TURN) fill(Preset.ANT_COLOR_HARD_TURN); else fill(Preset.ANT_COLOR);
    rectMode(CENTER);
    rect((float)pos.x,(float)pos.y,Preset.ANT_DRAW_SIZE,Preset.ANT_DRAW_SIZE);
    //rect((float)pos.x,(float)pos.y,2*Preset.ANT_DRAW_SIZE,2*Preset.ANT_DRAW_SIZE);
    
    if (Preset.ANT_SHOW_LOOK_AHEAD) {
      stroke(Preset.ANT_COLOR);
      line((float)pos.x,(float)pos.y,(float)(pos.x+Preset.ANT_LOOK_AHEAD_FACTOR*mov.x),(float)(pos.y+Preset.ANT_LOOK_AHEAD_FACTOR*mov.y));
    }
  }
  
  
}
