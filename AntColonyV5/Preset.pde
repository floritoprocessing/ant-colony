class Preset {
  
  
    
  /*****************************************************
  / ANT PRESETS: */
  
  // amount:
  public static final int ANT_AMOUNT = 1200;
  
  // behaviour
  public static final float ANT_SPEED = 3.0;
  public static final float ANT_SPEED_MIN_FOR_HARD_TURN = 0.4;
  public static final float ANT_LOOK_AHEAD_FACTOR = 6.0;
  public static final float ANT_SLOW_DOWN_FAC = 0.5;
  
  // display
  public static final boolean ANT_SHOW_LOOK_AHEAD = false;
  public static final int ANT_COLOR = 0xFF8C6640;
  public static final int ANT_COLOR_HARD_TURN = 0xFFFF0000;
  public static final float ANT_DRAW_SIZE = 3.0;
  
  /*****************************************************/
  
  
  
  
  
  /*****************************************************
  / OBSTACLE PRESETS: */
  
  //form:
  public static final int OBSTACLE_AMOUNT = 16;
  public static final int OBSTACLE_CORNER_POINTS = 9;
  public static final float OBSTACLE_MIN_RADIUS = 10;
  public static final float OBSTACLE_MAX_RADIUS = 65;
  public static final int OBSTACLE_COLOR = 0xFF612E03;
  
  
  public static final float OBSTACLE_ROT_SPEED_MIN = 0.002;
  public static final float OBSTACLE_ROT_SPEED_MAX = 0.005;
  
  /*****************************************************/
  
  
  
  
  
  public static final int COLOR_BACKGROUND = 0xFFFFFFFF;
  
  Preset() {
  }
  
}
