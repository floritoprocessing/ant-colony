import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class AntColonyV3 extends PApplet {int ANTS_AMOUNT=3000;
int FOOD_SOURCES_AMOUNT=1;
int FOOD_SOURCES_SIZE_MIN=8;
int FOOD_SOURCES_SIZE_MAX=15;

boolean show_ants=true;
boolean show_bases=true;

Ant[] ant;
Base home;
Vector foods=new Vector();

Ferhormone ferhormone;

public void setup() {
  size(640,480,P3D);
  
  // create ants
  ant=new Ant[ANTS_AMOUNT];
  for (int i=0;i<ANTS_AMOUNT;i++) ant[i]=new Ant(width,height);
  
  // create ferhormone
  ferhormone=new Ferhormone(width,height);
  
  // create home
  home=new Base((0.2f+random(0.6f))*width,(0.2f+random(0.6f))*height,10);
  
  // create foodsources
  for (int i=0;i<FOOD_SOURCES_AMOUNT;i++) {
    Base food=new Base((0.1f+random(0.8f))*width,(0.1f+random(0.8f))*height,random(FOOD_SOURCES_SIZE_MIN,FOOD_SOURCES_SIZE_MAX));
    food.setColor(random(192,255),random(192,255),random(192,255));
    foods.add(food);
  }
  
}



public void mousePressed() {
  // SHIFT+mouseclick creates new bases
  if (keyPressed) {
    if (key==CODED) {
      if (keyCode==SHIFT) {
        Base food=new Base(mouseX,mouseY,random(FOOD_SOURCES_SIZE_MIN,FOOD_SOURCES_SIZE_MAX));
        food.setColor(random(192,255),random(192,255),random(192,255));
        foods.add(food);
      }
    }
  }
}


public void keyPressed() {
  if (key=='a') show_ants=!show_ants;
  if (key=='b') show_bases=!show_bases;
}

public void draw() {
  background(255);
  
  if (mousePressed&&!keyPressed) {
    ferhormone.erase(mouseX,mouseY,8);
  }
  
  // draw ferhormone
  ferhormone.toScreen();
  
  // draw home
  if (show_bases) home.toScreen();
  
  // draw foodsources
  if (show_bases) {
    for (int i=0;i<foods.size();i++) {
      Base food=(Base)(foods.elementAt(i));
      food.toScreen();
    }
  }
  
  // move and draw ants
  for (int i=0;i<ANTS_AMOUNT;i++) {
    ant[i].move(home,foods,ferhormone);
    if (show_ants) ant[i].toScreen();
    if (ant[i].getMode()==Ant.MODE_GOING_HOME) ant[i].toFerhormone(ferhormone);
  }
  
  // evaporate ferhormones
  ferhormone.evaporate();
  
}

class Ant {

  int xlim, ylim;
  float x,y;
  float xm,ym;
  float spd,dir;

  float EAT_FROM_FOOD_AMOUNT=0.05f;  //0.2
  
  public static final int COLOR_SEARCHING=0x808080;
  public static final int COLOR_GOING_HOME=0xffffff;

  int col=COLOR_SEARCHING;

  public static final int MODE_SEARCHING=0;
  public static final int MODE_GOING_HOME=1;

  int mode=MODE_SEARCHING;


  Ant(int w, int h) {
    xlim=w;
    ylim=h;
    x=random(w);
    y=random(h);
    spd=random(1,2);
    dir=random(2*PI);
    xm=spd*cos(dir);
    ym=spd*sin(dir);
  }





  /* 
    THIS IS THE MAIN ANT MOVING ENGINE
    ----------------------------------
  */
  
  public void move(Base h, Vector fds, Ferhormone f) {

    /*
      LOOK FOR A PATH IN THE DIRECT SURROUNDING OF DOT
      CREATE AN AVERAGE DIRECTION FOR SURROUNDING PATHS
    */
    float xo=0, yo=0;
    if (x>=1&&x<xlim-1&&y>=1&&y<ylim-1) {
      for (int xa=-1;xa<=1;xa++) {
        for (int ya=-1;ya<=1;ya++) {
          float v=f.getAt(PApplet.toInt(x+xa),PApplet.toInt(y+ya));
          xo+=v*xa;
          yo+=v*ya;
        }
      }
    }
    int usePath=(xo!=0&&yo!=0)?1:0;
    float dirPath=atan2(yo,xo);
       
    
    /*
      IF ANT NEEDS TO MOVE HOME,
      CREATE A DIRECTION THAT POINTS TOWARDS HOME
    */
    float dxBase=0, dyBase=0;
    if (mode==MODE_GOING_HOME) {
      dxBase=h.x-x;
      dyBase=h.y-y;
    }
    float dirBase=atan2(dyBase,dxBase);
        
    
    /*
      CREATE A RANDOM DIRECTION
    */
    float dirRandom=random(2*PI);


    /*
      CREATE A NEW DIRECTION FROM:
      * PREVIOUS DIRECTION
      * AVERAGE PATH DIRECTION (if there is one)
      * HOME DIRECTION (if going home)
      * RANDOM DIRECTION
    */
    dir=mixDirections(dirPath,usePath*0.3f,dirBase,mode*0.05f,dirRandom,0.2f,dir,1); // x 0.5 0.5 1

    
    // direction to move vector
    xm=spd*cos(dir);
    ym=spd*sin(dir);

    // move position
    x+=xm;
    y+=ym;

    // make sure that position is within screen
    if (x>=xlim-1.0f) x-=xlim;
    if (x<0) x+=xlim;
    if (y>=ylim-1.0f) y-=ylim;
    if (y<0) y+=ylim;

    /*
      SWITCH TO MODE 'SEARCHING' IF ANT REACHED HOME
    */
    if (h.insideCircle(x,y)) switchMode(MODE_SEARCHING);

    /*
      SWITCH TO MODE 'GOING HOME' IF ANT REACHED FOOD
      REDUCE THE SIZE OF THE FOOD
    */
    for (int i=0;i<fds.size();i++) {
      Base fd=(Base)(fds.elementAt(i));
      if (fd.insideCircle(x,y)) {
        switchMode(MODE_GOING_HOME);
        fd.reduceSize(fds,EAT_FROM_FOOD_AMOUNT);
      }
    }
  }
  
  /* 
    END OF THE MAIN ANT MOVING ENGINE
    ---------------------------------
  */
  
  
  
  

  public void switchMode(int m) {
    mode=m;
    if (mode==MODE_SEARCHING) {
      col=COLOR_SEARCHING;
    } 
    else if (mode==MODE_GOING_HOME) {
      col=COLOR_GOING_HOME;
    }
  }

  public void toScreen() {
    set((int)x,(int)y,col);
  }

  public void toFerhormone(Ferhormone f) {
    f.traceAt((int)x,(int)y);
  }

  public int getMode() {
    return mode;
  }

  public float mixDirections(float dir1, float p1, float dir2, float p2, float dir3, float p3, float dir4, float p4) {
    float x1=cos(dir1);
    float y1=sin(dir1);
    float x2=cos(dir2);
    float y2=sin(dir2);    
    float x3=cos(dir3);
    float y3=sin(dir3);
    float x4=cos(dir4);
    float y4=sin(dir4);
    float x=p1*x1+p2*x2+p3*x3+p4*x4;
    float y=p1*y1+p2*y2+p3*y3+p4*y4;
    return atan2(y,x);
  }
}

class Base {
  
  float x, y;
  float x1,x2,y1,y2;
  float radius,rad2,A;
  int col=color(166,120,0);
  
  Base(float _x, float _y, float _r) {
    x=_x;
    y=_y;
    radius=_r;
    
    x1=x-radius;
    x2=x+radius;
    y1=y-radius;
    y2=y+radius;
    rad2=radius*radius;
    A=PI*rad2;
  }
  
  public void setColor(float r, float g, float b) {
    col=color(r,g,b);
  }
  
  public void reduceSize(Vector fds,float amount) {
    if (A>amount) {
      A-=amount;
      rad2=A/PI;
      radius=sqrt(rad2);
    } else {
      fds.remove(this);
    }
  }
  
  public void toScreen() {
    noStroke();
    fill(col);
    ellipseMode(CENTER_RADIUS);
    ellipse(x,y,radius,radius);
  }
  
  public boolean insideCircle(float px, float py) {
    boolean out=false;
    float dx=abs(px-x);
    if (dx<radius) {
      float dy=abs(py-y);
      if (dy<radius) {
        if (rad2>(dx*dx+dy*dy)) {
          out=true;
        }
      }
    }
    return out;
  }
  
}

class Ferhormone {
  int wid, hei;
  float[] display;
  
  Ferhormone(int w, int h) {
    wid=w;
    hei=h;
    display=new float[w*h];
    for (int i=0;i<display.length;i++) {
      display[i]=0;
    }
  }
  
  public void traceAt(int x, int y) {
    int i=y*wid+x;
    display[i]+=40;
    if (display[i]>255) display[i]=255;
  }
  
  public float getAt(int x, int y) {
    int i=y*wid+x;
    return display[i];
  }
  
  public void erase(int x, int y, int r) {
    for (int i=-r;i<=r;i++) {
      for (int j=-r;j<=r;j++) {
        int nx=x+i;
        int ny=y+j;
        if (nx>=0&&nx<wid&&ny>=0&&ny<hei) {
          int ix=ny*wid+nx;
          display[ix]=0;
        }
      }
    }
  }
  
  public void evaporate() {
    for (int i=0;i<display.length;i++) {
      display[i]/=1.004f; //1.005
    }
  }
  
  public void toScreen() {
    loadPixels();
    for (int i=0;i<display.length;i++) {
      int b=PApplet.toInt(display[i]);
      pixels[i]=b<<16|b<<8|b;
    }
    updatePixels();
  }
  
}
static public void main(String args[]) {   PApplet.main(new String[] { "AntColonyV3" });}}