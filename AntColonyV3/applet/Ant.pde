class Ant {

  int xlim, ylim;
  float x,y;
  float xm,ym;
  float spd,dir;

  float EAT_FROM_FOOD_AMOUNT=0.05;  //0.2
  
  public static final int COLOR_SEARCHING=0x808080;
  public static final int COLOR_GOING_HOME=0xffffff;

  color col=COLOR_SEARCHING;

  public static final int MODE_SEARCHING=0;
  public static final int MODE_GOING_HOME=1;

  int mode=MODE_SEARCHING;


  Ant(int w, int h) {
    xlim=w;
    ylim=h;
    x=random(w);
    y=random(h);
    spd=random(1,2);
    dir=random(2*PI);
    xm=spd*cos(dir);
    ym=spd*sin(dir);
  }





  /* 
    THIS IS THE MAIN ANT MOVING ENGINE
    ----------------------------------
  */
  
  void move(Base h, Vector fds, Ferhormone f) {

    /*
      LOOK FOR A PATH IN THE DIRECT SURROUNDING OF DOT
      CREATE AN AVERAGE DIRECTION FOR SURROUNDING PATHS
    */
    float xo=0, yo=0;
    if (x>=1&&x<xlim-1&&y>=1&&y<ylim-1) {
      for (int xa=-1;xa<=1;xa++) {
        for (int ya=-1;ya<=1;ya++) {
          float v=f.getAt(int(x+xa),int(y+ya));
          xo+=v*xa;
          yo+=v*ya;
        }
      }
    }
    int usePath=(xo!=0&&yo!=0)?1:0;
    float dirPath=atan2(yo,xo);
       
    
    /*
      IF ANT NEEDS TO MOVE HOME,
      CREATE A DIRECTION THAT POINTS TOWARDS HOME
    */
    float dxBase=0, dyBase=0;
    if (mode==MODE_GOING_HOME) {
      dxBase=h.x-x;
      dyBase=h.y-y;
    }
    float dirBase=atan2(dyBase,dxBase);
        
    
    /*
      CREATE A RANDOM DIRECTION
    */
    float dirRandom=random(2*PI);


    /*
      CREATE A NEW DIRECTION FROM:
      * PREVIOUS DIRECTION
      * AVERAGE PATH DIRECTION (if there is one)
      * HOME DIRECTION (if going home)
      * RANDOM DIRECTION
    */
    dir=mixDirections(dirPath,usePath*0.3,dirBase,mode*0.05,dirRandom,0.2,dir,1); // x 0.5 0.5 1

    
    // direction to move vector
    xm=spd*cos(dir);
    ym=spd*sin(dir);

    // move position
    x+=xm;
    y+=ym;

    // make sure that position is within screen
    if (x>=xlim-1.0) x-=xlim;
    if (x<0) x+=xlim;
    if (y>=ylim-1.0) y-=ylim;
    if (y<0) y+=ylim;

    /*
      SWITCH TO MODE 'SEARCHING' IF ANT REACHED HOME
    */
    if (h.insideCircle(x,y)) switchMode(MODE_SEARCHING);

    /*
      SWITCH TO MODE 'GOING HOME' IF ANT REACHED FOOD
      REDUCE THE SIZE OF THE FOOD
    */
    for (int i=0;i<fds.size();i++) {
      Base fd=(Base)(fds.elementAt(i));
      if (fd.insideCircle(x,y)) {
        switchMode(MODE_GOING_HOME);
        fd.reduceSize(fds,EAT_FROM_FOOD_AMOUNT);
      }
    }
  }
  
  /* 
    END OF THE MAIN ANT MOVING ENGINE
    ---------------------------------
  */
  
  
  
  

  void switchMode(int m) {
    mode=m;
    if (mode==MODE_SEARCHING) {
      col=COLOR_SEARCHING;
    } 
    else if (mode==MODE_GOING_HOME) {
      col=COLOR_GOING_HOME;
    }
  }

  void toScreen() {
    set((int)x,(int)y,col);
  }

  void toFerhormone(Ferhormone f) {
    f.traceAt((int)x,(int)y);
  }

  int getMode() {
    return mode;
  }

  float mixDirections(float dir1, float p1, float dir2, float p2, float dir3, float p3, float dir4, float p4) {
    float x1=cos(dir1);
    float y1=sin(dir1);
    float x2=cos(dir2);
    float y2=sin(dir2);    
    float x3=cos(dir3);
    float y3=sin(dir3);
    float x4=cos(dir4);
    float y4=sin(dir4);
    float x=p1*x1+p2*x2+p3*x3+p4*x4;
    float y=p1*y1+p2*y2+p3*y3+p4*y4;
    return atan2(y,x);
  }
}
