class Base {
  
  float x, y;
  float x1,x2,y1,y2;
  float radius,rad2,A;
  color col=color(166,120,0);
  
  Base(float _x, float _y, float _r) {
    x=_x;
    y=_y;
    radius=_r;
    
    x1=x-radius;
    x2=x+radius;
    y1=y-radius;
    y2=y+radius;
    rad2=radius*radius;
    A=PI*rad2;
  }
  
  void setColor(float r, float g, float b) {
    col=color(r,g,b);
  }
  
  void reduceSize(Vector fds,float amount) {
    if (A>amount) {
      A-=amount;
      rad2=A/PI;
      radius=sqrt(rad2);
    } else {
      fds.remove(this);
    }
  }
  
  void toScreen() {
    noStroke();
    fill(col);
    ellipseMode(CENTER);
    ellipse(x,y,radius,radius);
  }
  
  boolean insideCircle(float px, float py) {
    boolean out=false;
    float dx=abs(px-x);
    if (dx<radius) {
      float dy=abs(py-y);
      if (dy<radius) {
        if (rad2>(dx*dx+dy*dy)) {
          out=true;
        }
      }
    }
    return out;
  }
  
}
