class Ferhormone {
  int wid, hei;
  float[] display;
  
  Ferhormone(int w, int h) {
    wid=w;
    hei=h;
    display=new float[w*h];
    for (int i=0;i<display.length;i++) {
      display[i]=0;
    }
  }
  
  void traceAt(int x, int y) {
    int i=y*wid+x;
    display[i]+=40;
    if (display[i]>255) display[i]=255;
  }
  
  float getAt(int x, int y) {
    int i=y*wid+x;
    return display[i];
  }
  
  void erase(int x, int y, int r) {
    for (int i=-r;i<=r;i++) {
      for (int j=-r;j<=r;j++) {
        int nx=x+i;
        int ny=y+j;
        if (nx>=0&&nx<wid&&ny>=0&&ny<hei) {
          int ix=ny*wid+nx;
          display[ix]=0;
        }
      }
    }
  }
  
  void evaporate() {
    for (int i=0;i<display.length;i++) {
      display[i]/=1.004; //1.005
    }
  }
  
  void toScreen() {
    loadPixels();
    for (int i=0;i<display.length;i++) {
      int b=int(display[i]);
      pixels[i]=b<<16|b<<8|b;
    }
    updatePixels();
  }
  
}
