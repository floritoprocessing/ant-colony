import java.util.Vector;

int ANTS_AMOUNT=3000;
int FOOD_SOURCES_AMOUNT=1;
int FOOD_SOURCES_SIZE_MIN=8;
int FOOD_SOURCES_SIZE_MAX=15;

boolean show_ants=true;
boolean show_bases=true;

Ant[] ant;
Base home;
Vector foods=new Vector();

Ferhormone ferhormone;

void setup() {
  size(640,480,P3D);
  
  // create ants
  ant=new Ant[ANTS_AMOUNT];
  for (int i=0;i<ANTS_AMOUNT;i++) ant[i]=new Ant(width,height);
  
  // create ferhormone
  ferhormone=new Ferhormone(width,height);
  
  // create home
  home=new Base((0.2+random(0.6))*width,(0.2+random(0.6))*height,10);
  
  // create foodsources
  for (int i=0;i<FOOD_SOURCES_AMOUNT;i++) {
    Base food=new Base((0.1+random(0.8))*width,(0.1+random(0.8))*height,random(FOOD_SOURCES_SIZE_MIN,FOOD_SOURCES_SIZE_MAX));
    food.setColor(random(192,255),random(192,255),random(192,255));
    foods.add(food);
  }
  
  println("[a] toggle showing ants");
  println("[b] toggle showing bases");
  println("[shift]-click to create new base");
}



void mousePressed() {
  // SHIFT+mouseclick creates new bases
  if (keyPressed) {
    if (key==CODED) {
      if (keyCode==SHIFT) {
        Base food=new Base(mouseX,mouseY,random(FOOD_SOURCES_SIZE_MIN,FOOD_SOURCES_SIZE_MAX));
        food.setColor(random(192,255),random(192,255),random(192,255));
        foods.add(food);
      }
    }
  }
}


void keyPressed() {
  if (key=='a') show_ants=!show_ants;
  if (key=='b') show_bases=!show_bases;
}

void draw() {
  background(255);
  
  if (mousePressed&&!keyPressed) {
    ferhormone.erase(mouseX,mouseY,8);
  }
  
  // draw ferhormone
  ferhormone.toScreen();
  
  // draw home
  if (show_bases) home.toScreen();
  
  // draw foodsources
  if (show_bases) {
    for (int i=0;i<foods.size();i++) {
      Base food=(Base)(foods.elementAt(i));
      food.toScreen();
    }
  }
  
  // move and draw ants
  for (int i=0;i<ANTS_AMOUNT;i++) {
    ant[i].move(home,foods,ferhormone);
    if (show_ants) ant[i].toScreen();
    if (ant[i].getMode()==Ant.MODE_GOING_HOME) ant[i].toFerhormone(ferhormone);
  }
  
  // evaporate ferhormones
  ferhormone.evaporate();
  
}
