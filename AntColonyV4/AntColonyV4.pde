
Ant[] ant = new Ant[10000];
Location home = new Location();
Location food = new Location();
Background canvas = new Background();

boolean SHOW_ANTS = true;

void setup() {
  size(200,200,P3D);
  background(0);
  canvas.setDimensions(width,height);
  home.setPosition((int)(width*random(0.2,0.8)),(int)(height*random(0.2,0.8)));
  food.setPosition((int)(width*random(0.2,0.8)),(int)(height*random(0.2,0.8)));
  food.isBase=false;
  for (int i=0;i<ant.length;i++) {
    ant[i] = new Ant();
    ant[i].setPosition(home.getPosition());
    ant[i].setPosition(new Vec2D((int)(width*random(0.2,0.8)),(int)(height*random(0.2,0.8))));
  }
}

void keyPressed() {
  if (key=='a'||key=='A') SHOW_ANTS=!SHOW_ANTS;
}

void draw() {
  if (random(1.0)<0.5) canvas.reduce();
  canvas.show();
  home.show();
  food.show();
  //for (int i=0;i<ant.length;i++) ant[i].smellAt(canvas);
  for (int i=0;i<ant.length;i++) ant[i].move(canvas);
  for (int i=0;i<ant.length;i++) ant[i].draw(canvas);
  if (SHOW_ANTS) for (int i=0;i<ant.length;i++) ant[i].show();
}
