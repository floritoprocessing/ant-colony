class Background {
  PImage img;
  int wid=1, hei=1;
  int DRAW_PRESSURE=16;
  
  Background() {
  }
  
  void setDimensions(int _w, int _h) {
    img = new PImage(_w,_h);
    wid=_w;
    hei=_h;
  }
  
  void setAt(int _x, int _y, float ferhormoneLevel) {
    int b = (int)red(get(_x,_y));
    int dp = b + (int)(ferhormoneLevel*DRAW_PRESSURE);
    img.set(_x,_y,color(dp,dp,dp));
  }
  
  int getSmellyness(int _x,int _y) {
    return (int)brightness(img.get(_x,_y));
  }
  
  void show() {
    image(img,0,0);
  }
  
  void reduce() {
    for (int x=0;x<wid;x++) {
      for (int y=0;y<hei;y++) {
        int b = (int)brightness(img.get(x,y));
        b-=1;
        if (b<0) b=0;
        img.set(x,y,color(b,b,b));
      }
    }
  }
}
