class Vec2D {
  int x=0,y=0;
  
  Vec2D() {
  }
  
  Vec2D(int _x, int _y) {
    x=_x;
    y=_y;
  }
  
  void set(int _x, int _y) {
    x=_x;
    y=_y;
  }
  
  void set(Vec2D v) {
    x=v.x;
    y=v.y;
  }
  
  void add(Vec2D v) {
    x+=v.x;
    y+=v.y;
  }
  
  boolean isNullVec() {
    return (x==0&&y==0);
  }
  
  void rotate45RightOrLeft(boolean right) {
    float rd=(right?-1:1)*PI/4.0;
    float SIN=sin(rd);
    float COS=cos(rd); 
    float xn=x*COS-y*SIN; 
    float yn=y*COS+x*SIN;
    x=(int)round(xn);
    y=(int)round(yn);
  }
  
  String toString() {
    return (x+"/"+y);
  }
  
}
