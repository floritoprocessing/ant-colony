class Ant {
  Vec2D position = new Vec2D();
  Vec2D direction = new Vec2D();
  boolean trace=false;
  float ferhormoneLevel = 0;
  
  Ant() {
    makeRandomDirection();
  }
  
  void makeRandomDirection() {
    int xd = 0;
    int yd = 0;
    while (xd==0&&yd==0) {
      xd = (int)random(3)-1;
      yd = (int)random(3)-1;
    }
    direction.set(xd,yd);
  }
  
  void changeDirection() {
    direction.rotate45RightOrLeft(random(1.0)<0.5);
  }
  
  void setPosition(Vec2D v) {
    position.set(v);
  }
  
  int getColor(int _x, int _y) {
    color c=get(_x,_y);
    int out = (c>>16&0xFF)<<16|(c>>8&0xFF)<<8|(c&0xFF);
    return out;
  }
  
  /*
  void smellAt(Background c) {
    Vec2D smelliestDirection=new Vec2D();
    int mostSmell=0;
    for (int ox=-1;ox<=1;ox++) {
      for (int oy=-1;oy<=1;oy++) {
        int spx=position.x+ox;
        int spy=position.y+oy;
        int smellyness=c.getSmellyness(spx,spy);
        if (smellyness>mostSmell) {
          mostSmell=smellyness;
          smelliestDirection.set(ox,oy);
        }
      }
    }
    if (!smelliestDirection.isNullVec()) {
      ferhormoneLevel-=0.01;  
      if (random(512)<mostSmell) direction.set(smelliestDirection);
    } else {
      ferhormoneLevel-=0.03;
    }
    
    if (ferhormoneLevel<0) ferhormoneLevel=0;
  }
  */
  
  void move(Background c) {
    
    // don't change direction if target path has ferhormones
    int smellynessAtNextStep = c.getSmellyness(position.x+direction.x,position.y+direction.y);
    if (smellynessAtNextStep>0) {
      // do nothing
      ferhormoneLevel-=0.0002;
      if (random(512)>smellynessAtNextStep) changeDirection();
    } else {
      // random movement:
      ferhormoneLevel-=0.001;
      if (random(1.0)<0.05) changeDirection();
    }
    
    position.add(direction);
     
    // edge avoidance:
    if (position.x<0) position.x+=c.wid;
    if (position.x>=c.wid) position.x-=c.wid;
    if (position.y<0) position.y+=c.hei;
    if (position.y>=c.hei) position.y-=c.hei;
    
    // locate base/food:
    int gc = getColor(position.x,position.y);
    if (gc==Location.FOOD_COLOR) ferhormoneLevel=1.0;
    if (gc==Location.BASE_COLOR) ferhormoneLevel=0.0;
    
    if (ferhormoneLevel<0) ferhormoneLevel=0;
  }
  
  void draw(Background c) {
    c.setAt(position.x,position.y,ferhormoneLevel);
  }
  
  void show() {
    set(position.x,position.y,trace?Location.FOOD_COLOR:Location.BASE_COLOR);
  }
  
}
