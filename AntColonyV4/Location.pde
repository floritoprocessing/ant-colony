class Location {

  public static final int BASE_COLOR = 0x0000FF;
  public static final int FOOD_COLOR = 0x00FFFF;

  Vec2D position = new Vec2D();
  boolean isBase=true;

  Location() {
  }

  void setPosition(int _x, int _y) {
    position.set(_x,_y);
  }

  Vec2D getPosition() {
    return position;
  }

  void show() {
    for (int ox=-3;ox<=3;ox++) {
      for (int oy=-3;oy<=3;oy++) {
        if (isBase) {
          set(position.x+ox,position.y+oy,BASE_COLOR);
        } 
        else {
          set(position.x+ox,position.y+oy,FOOD_COLOR);
        }
      }
    }
  }
}
